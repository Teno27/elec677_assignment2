# README #

There are two source code files:

(1) cnn_cifar10_final.py: this is for problem 1 “Visualizing a CNN with CIFAR10”. To run the code, we should put the image folder CIFAR10 under the same directory with the cnn_cifar10_final.py, but not the home directory. (At line 39 and 46 of the source code, I changed the path to current directory). It will generate a “results” folder under the same directory after running the code, which can be used by tensorboard to visualize the training and testing accuracy/loss, statistics of activations, and visualization of weights.

(2) rnn_mnist_final.py: this is for problem 3 “Build and Train an RNN on MNIST”. My code supports three modes: BASIC, LSTM, GRU, by configuring line 15 of the code. It will generate a “results” folder under the same directory after running the code, which can be used by tensorboard to visualize the training and testing accuracy/loss as required.