from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from scipy import misc
import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib as mp

import argparse
import os
import tensorflow as tf


def main():

    result_dir = './results/'  # directory where the results from the training are saved

    ntrain = 1000  # per class
    ntest = 100  # per class
    nclass = 10  # number of classes
    imsize = 28
    nchannels = 1
    batchsize = 100
    nsamples = ntrain * nclass
    max_step = 10000

    Train = np.zeros((ntrain * nclass, imsize, imsize, nchannels))
    Test = np.zeros((ntest * nclass, imsize, imsize, nchannels))
    LTrain = np.zeros((ntrain * nclass, nclass))
    LTest = np.zeros((ntest * nclass, nclass))

    itrain = -1
    itest = -1

    for iclass in range(0, nclass):
        for isample in range(0, ntrain):
            path = './CIFAR10/Train/%d/Image%05d.png' % (iclass, isample)
            im = misc.imread(path);  # 28 by 28
            im = im.astype(float) / 255
            itrain += 1
            Train[itrain, :, :, 0] = im
            LTrain[itrain, iclass] = 1  # 1-hot lable
        for isample in range(0, ntest):
            path = './CIFAR10/Test/%d/Image%05d.png' % (iclass, isample)
            im = misc.imread(path);  # 28 by 28
            im = im.astype(float) / 255
            itest += 1
            Test[itest, :, :, 0] = im
            LTest[itest, iclass] = 1  # 1-hot lable


    if tf.gfile.Exists(result_dir):
        tf.gfile.DeleteRecursively(result_dir)
    tf.gfile.MakeDirs(result_dir)

    sess = tf.InteractiveSession()

    # Create a multilayer model.

    # Input placeholders
    with tf.name_scope('input'):
        tf_data = tf.placeholder(tf.float32, [None, 28, 28, 1], name='x-input')  # tf variable for the data, remember shape is [None, width, height, numberOfChannels]
        tf_labels = tf.placeholder(tf.float32, [None, 10], name='y-input')  # tf variable for labels
    # We can't initialize these variables to 0 - the network will get stuck.

    def weight_variable(shape):
        initial = tf.truncated_normal(shape, stddev=0.1)
        W = tf.Variable(initial)
        return W

    def bias_variable(shape):
        initial = tf.constant(0.1, shape=shape)
        b = tf.Variable(initial)
        return b

    def conv2d(x, W):
        h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')
        return h_conv

    def max_pool_2x2(x):
        h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                               strides=[1, 2, 2, 1], padding='SAME')
        return h_max

    def variable_summaries(var, name):
        """Attach a lot of summaries to a Tensor."""
        with tf.name_scope('summaries'):
            mean = tf.reduce_mean(var)
            tf.scalar_summary('mean/' + name, mean)
            with tf.name_scope('stddev'):
                stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
            tf.scalar_summary('stddev/' + name, stddev)
            tf.scalar_summary('max/' + name, tf.reduce_max(var))
            tf.scalar_summary('min/' + name, tf.reduce_min(var))
            tf.histogram_summary(name, var)

    def nn_layer(input_tensor, wshape, bshape, layer_name, act=tf.nn.relu):
        """Reusable code for making a simple neural net layer.
    It does a matrix multiply, bias add, and then uses relu to nonlinearize.
    It also sets up name scoping so that the resultant graph is easy to read,
    and adds a number of summary ops.
    """
        # Adding a name scope ensures logical grouping of the layers in the graph.
        with tf.name_scope(layer_name):
            # This Variable will hold the state of the weights for the layer
            with tf.name_scope('weights'):
                weights = weight_variable(wshape)
                variable_summaries(weights, layer_name + '/weights')
                # Visualize weight for first conv layer
                if layer_name == 'convlayer1':
                    for k in range(32):
                        tf_weights = tf.reshape(weights[:, :, :, k], [1, 5, 5, 1])
                        tf.image_summary("weights%d" % k, tf_weights, max_images=1)
            with tf.name_scope('biases'):
                biases = bias_variable(bshape)
                variable_summaries(biases, layer_name + '/biases')
            with tf.name_scope('net_input'):
                preactivate = conv2d(input_tensor, weights) + biases
                variable_summaries(preactivate, layer_name + '/net_input')
            with tf.name_scope('activations'):
                activations = act(preactivate)
                variable_summaries(activations, layer_name + '/activations')
            with tf.name_scope('maxpool'):
                pool = max_pool_2x2(activations)
                variable_summaries(pool, layer_name + '/maxpool')
        return pool

    def ds_layer(input_tensor, wshape, bshape, pshape, layer_name, act=tf.nn.relu):
        # Adding a name scope ensures logical grouping of the layers in the graph.
        with tf.name_scope(layer_name):
            # This Variable will hold the state of the weights for the layer
            with tf.name_scope('weights'):
                weights = weight_variable(wshape)
                variable_summaries(weights, layer_name + '/weights')
            with tf.name_scope('biases'):
                biases = bias_variable(bshape)
                variable_summaries(biases, layer_name + '/biases')
            # with tf.name_scope('poolflat'):
            poolflat = tf.reshape(input_tensor, pshape)
            #  variable_summaries(poolflat, layer_name + '/poolflat')
            with tf.name_scope('net_input'):
                preactivate = tf.matmul(poolflat, weights) + biases
                variable_summaries(preactivate, layer_name + '/net_input')
            with tf.name_scope('activations'):
                activations = act(preactivate)
                variable_summaries(activations, layer_name + '/activations')
        return activations

    h_pool1 = nn_layer(tf_data, [5, 5, 1, 32], [32], 'convlayer1')

    h_pool2 = nn_layer(h_pool1, [5, 5, 32, 64], [64], 'convlayer2')

    h_fc1 = ds_layer(h_pool2, [7 * 7 * 64, 1024], [1024], [-1, 7 * 7 * 64], 'denselayer')

    with tf.name_scope('dropout'):
        keep_prob = tf.placeholder(tf.float32)
        tf.scalar_summary('dropout_keep_probability', keep_prob)
        dropped = tf.nn.dropout(h_fc1, keep_prob)

    # softmax
    with tf.name_scope('softmax'):
        with tf.name_scope('weights'):
            W_fc2 = weight_variable([1024, 10])
            variable_summaries(W_fc2, 'softmax' + '/weights')
        with tf.name_scope('biases'):
            b_fc2 = bias_variable([10])
            variable_summaries(b_fc2, 'softmax' + '/biases')
        with tf.name_scope('net_input'):
            net_fc2 = tf.matmul(dropped, W_fc2) + b_fc2
            variable_summaries(net_fc2, 'softmax' + '/net_input')

    y = tf.nn.softmax(net_fc2)

    with tf.name_scope('cross_entropy'):

        cross_entropy = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y), reduction_indices=[1]))
        tf.scalar_summary('cross entropy', cross_entropy)

    with tf.name_scope('train'):
        optimizer = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(tf_labels, 1))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        tf.scalar_summary('accuracy', accuracy)

    # Merge all the summaries and write them out
    summary_op = tf.merge_all_summaries()
    train_writer = tf.train.SummaryWriter(result_dir + '/train', sess.graph)
    test_writer = tf.train.SummaryWriter(result_dir + '/test', sess.graph)
    tf.initialize_all_variables().run()

    # Batch initialization
    batch_xs = np.zeros((batchsize, imsize, imsize, nchannels))  # setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
    batch_ys = np.zeros((batchsize, nclass))  # setup as [batchsize, the how many classes]

    for i in range(max_step):  # try a small iteration size once it works then continue
        # Training
        perm = np.arange(nsamples)
        np.random.shuffle(perm)
        for j in range(batchsize):
            batch_xs[j, :, :, :] = Train[perm[j], :, :, :]
            batch_ys[j, :] = LTrain[perm[j], :]
        if i % 100 == 0:
            # calculate train accuracy and print it
            train_accuracy = accuracy.eval(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0})
            print("step %d, training accuracy %g" % (i, train_accuracy))
            # monitor training loss
            summary_str = sess.run(summary_op, feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5})
            train_writer.add_summary(summary_str, i)
            train_writer.flush()

            # calculate testing accuracy and print it
            test_accuracy = accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
            print("step %d, testing accuracy %g" % (i, test_accuracy))
            # monitor testing loss
            summary_str2 = sess.run(summary_op, feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 0.5})
            test_writer.add_summary(summary_str2, i)
            test_writer.flush()

        optimizer.run(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5})  # dropout only during training


if __name__ == "__main__":
    main()