import tensorflow as tf
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np
import os
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('MNIST_data', one_hot = True) #call mnist function
sess = tf.InteractiveSession()

learningRate = 0.001
trainingIters = 100000
batchSize = 100
displayStep = 10

mode = 'LSTM' # mode: BASIC, LSTM, GRU

nInput = 28 #we want the input to take the 28 pixels
nSteps = 28 #every 28
nHidden = 100 #number of neurons for the RNN
nClasses = 10 #this is MNIST so you know

result_dir = './results/'
if tf.gfile.Exists(result_dir):
    tf.gfile.DeleteRecursively(result_dir)
tf.gfile.MakeDirs(result_dir)

if mode == 'LSTM':
    nInitSize = 2*nHidden
else:
    nInitSize = nHidden

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])
istate = tf.placeholder("float", [None, nInitSize])

weights = {
    'hidden': tf.Variable(tf.random_normal([nInput, nHidden])),
    'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
    'hidden': tf.Variable(tf.random_normal([nHidden])),
    'out': tf.Variable(tf.random_normal([nClasses]))
}


def RNN(_X, _istate, _weights, _biases):
    _X = tf.transpose(_X, [1, 0, 2])
    _X = tf.reshape(_X, [-1, nInput])
    _X = tf.matmul(_X, _weights['hidden']) + _biases['hidden']

    if mode == 'BASIC':
        rnnCell = rnn_cell.BasicRNNCell(nHidden)
    elif mode == 'LSTM':
        rnnCell = rnn_cell.BasicLSTMCell(nHidden, forget_bias=1.0)
    elif mode == 'GRU':
        rnnCell = rnn_cell.GRUCell(nHidden)
    else:
        print ("Not supported RNN mode.")

    _X = tf.split(0, nSteps, _X)
    outputs, states = rnn.rnn(rnnCell, _X, initial_state=_istate)
    return tf.matmul(outputs[-1], _weights['out']) + _biases['out']

pred = RNN(x, istate, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learningRate).minimize(cost)
correctPred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))


# Add a scalar summary for the snapshot loss.
tf.scalar_summary('cost', cost)
tf.scalar_summary('accuracy', accuracy)
# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()
# Instantiate a SummaryWriter to output summaries and the Graph.
train_writer = tf.train.SummaryWriter(result_dir+ '/train', sess.graph)
test_writer = tf.train.SummaryWriter(result_dir+ '/test', sess.graph)

init = tf.initialize_all_variables()

# Test data
testLen = 500
testData = mnist.test.images[:testLen].reshape((-1, nSteps, nInput))
testLabel = mnist.test.labels[:testLen]

with tf.Session() as sess:
    sess.run(init)
    step = 1
    while step * batchSize < trainingIters:
        batchX, batchY = mnist.train.next_batch(batchSize)
        batchX = batchX.reshape((batchSize, nSteps, nInput))
        sess.run(optimizer, feed_dict={x: batchX, y: batchY, istate: np.zeros((batchSize, nInitSize))})
        if step % displayStep == 0:
            # Training
            acc = sess.run(accuracy, feed_dict={x: batchX, y: batchY, istate: np.zeros((batchSize, nInitSize))})
            loss = sess.run(cost, feed_dict={x: batchX, y: batchY, istate: np.zeros((batchSize, nInitSize))})
            print ("Iter " + str(step*batchSize) + ", Minibatch Loss= " + "{:.6f}".format(loss) + ", Training Accuracy= " + "{:.5f}".format(acc))
            summary_str1 = sess.run(summary_op, feed_dict={x: batchX, y: batchY, istate: np.zeros((batchSize, nInitSize))})
            train_writer.add_summary(summary_str1, (step * batchSize))
            train_writer.flush()

            #Testing
            test_accuracy = accuracy.eval(feed_dict={x: testData, y: testLabel, istate: np.zeros((testLen, nInitSize))})
            print("Iter " + str(step*batchSize) + ", Testing accuracy %g" % (test_accuracy))
            summary_str2 = sess.run(summary_op, feed_dict={x: testData, y: testLabel, istate: np.zeros((testLen, nInitSize))})
            test_writer.add_summary(summary_str2, (step * batchSize))
            test_writer.flush()
        step += 1

    print ("Optimization Finished")